#!/usr\/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
import datetime
import math
import hashlib
import random
import os
import json
import urllib
import urllib2
import zipfile

from boto.s3.connection import S3Connection
from boto.sqs.connection import SQSConnection
from boto.sqs.message import Message
import storages.backends.s3boto
import requests


from django.db import models
from django.core.urlresolvers import reverse

from django.contrib.auth.models import AbstractUser  # , User

from compute.lib import create_job
from wioframework.amodels import *
from wioframework import decorators as dec
#from science.models import Algorithm
#from references.models import Image
from extfields.fields import RefImageField


import settings


MODELS = []


##########################################################################
# EVALUATION PROTOCOL ARE VERY SPECIFIC TYPES OF ASSETS
##########################################################################

##
# NOT READY FOR JUNE 2013
##

NOT_STARTED = 0
IN_PROGRESS = 1
DONE = 2
STATUS = ((0, NOT_STARTED), (1, IN_PROGRESS), (2, DONE))


@wideiomodel
class DependencyFile(models.Model):
    url = models.URLField()
    #dataset = models.ForeignKey('evaluation.Dataset', related_name="dependencies")
    state = models.IntegerField(default=0, choices=STATUS)

    def on_add(self, request):
        create_job(request, "wideio_download_dataset", self.id)

    class WIDEIO_Meta:
        form_exclude = ['state']


MODELS.append(DependencyFile)
MODELS += get_dependent_models(DependencyFile)

#~ @wideio_followable("RelFollowDataset")


@wideio_owned()
@wideio_moderated()
@wideio_timestamped
@wideiomodel
class Dataset(models.Model):

    """
    A dataset is cloud data that is hosted on our platform - on the cloud.
    Note that the part that is hosted on our platform may be only a scraper
    that is then linked to the real data.
    """
    ##
    # image=models.ImageField(upload_to="models/datasets/") #<! an image to
    # make it look nice
    image = models.ForeignKey('references.Image', null=True)
    name = models.CharField(
        max_length=200,
        db_index=True)  # <! the name of the asset
    description = models.TextField()

    # Publicity and legal issues
    ##
    is_publicly_usable = models.BooleanField(default=False)
    # < non publicly viewable must be encrypted on disk and must not be normally viewable by staff
    is_publicly_viewable = models.BooleanField(default=False)
    require_safe_model = models.BooleanField(default=False)

    # Essential metadata
    ##
    #from science.models import License
    license = models.ForeignKey(
        'software.License',
        null=True)  # <legal text specifying access to the
    # dataset_assembly=models.ForeignKey(AssemblyFile) #< ! STANDARDIZED NAME : "dataset.dll"
    #from science.models import Datatype
    key_datatype = models.ForeignKey(
        'superglue.TypeInfo',
        related_name="dataset_key_dtype_rel",
        null=True,
        blank=True)
    data_datatype = models.ForeignKey(
        'superglue.TypeInfo',
        related_name="dataset_data_dtype_rel",
        null=True,
        blank=True)
    megabytes_on_disk = models.IntegerField(null=True, blank=True)

    # metadata provided on each entry
    ##
    # metadata_info=ListField(models.TextField, null=True,blank=True) # < NYI

    ##
    # Related to the application of a transform on a dataset
    ##
    # if a dataset is the result of the application of a function on the
    # dataset
    parent = models.ForeignKey('Dataset', null=True)
    # functional description of the transformation that has been done
    transform = models.TextField(null=True)
    # VIRTUAL EXTENSIONS ARE FEATURE DATASETS (IE DATASETS THAT HAVE BEEN COMPUTED BASED ON THE CURRENT DATASET)
    # ComputedFeatures=models.ManyToManyField("CloudDataset")

    # TBC
    # SOURCE OF THE INFORMATION
    # NOT UPDATABLE !
    # ELSE WE CANNOT BE SURE OF THE ASSOCIATIONS WITH RESULTS
    ##
    # dataset_zip = models.FileField(null=True, blank=True, upload_to=lambda instance, filename: os.path.join(str(instance.id), "__dataset.zip"),
    #                                                  **({'storage': storages.backends.s3boto.S3BotoStorage(bucket='wideiodatasets')} if settings.AWS_READY else {}))
    dataset_url = models.URLField(null=True, blank=True)
    default_request = models.TextField()
    #    blobid= models.TextField() < NOT USED
    # < The following should embed "Sellable Asset"
    ###
    pricing = models.IntegerField()  # < Amount in WIDE IO credits

    class Meta:
        db_table = "datasets"

    class WIDEIO_Meta:
        #~ from django.db.models import Q
        #~ default_query=lambda r:(Q(is_publicly_viewable=True)|Q(owner=r.user)|r.user.is_superuser)
        default_query = {'is_publicly_viewable': True}
        form_exclude = ['owner', 'metadata_info']
        form_fieldgroups = [
            ('basic_info', [
                'name', 'description', 'image', 'dataset_zip']), ('security_options', [
                    'is_publicly_usable', 'is_publicly_viewable', 'require_safe_model']), ('details', [
                        'license', 'pricing', 'default_request']), ]
        permissions = dec.perm_read_logged_users_write_for_admin_only

        class Actions:

            @wideio_action(
                icon="icon-cogs",
                possible=lambda o,
                r: True,
                mimetype="text/html")
            def download_file(self, request):
                return render_to_response(
                    "templates/evaluation/download_file.html", {'dataset', self})

    def __unicode__(self):
        return self.name

    def on_add(self, request):
        self.image.fix()

        from django.db.models import Q
        self.WIDEIO_Meta.default_query = staticmethod(
            lambda r: (Q(is_publicly_viewable=True) | Q(owner=r.user) or r.user.is_superuser))
        self.save()
        # ZIP FILE
        if self.dataset_zip is not None:
            result = requests.post(
                settings.DATASET_SERVER_URL +
                '/Home/' +
                self.id +
                '/UploadZip',
                files={
                    'dataset': self.dataset_zip})
            print result.text
        if self.dataset_url is not None:
            pass
        # GIT ASSEMBLY

    def on_view(self, request):
        if not self.is_usable():
            return {}
        error = ''
        _cache = {}  # < FIXME : we need a cache mechanism
        if 'keys' not in _cache:
            params = urllib.urlencode({'query': self.default_request})
            ret = urllib.urlopen(
                settings.DATASET_SERVER_URL +
                "Home/" +
                self.id +
                "/Keys/",
                params).read()
            print ret
            keys_jsonresponse = json.loads(ret)
            error = keys_jsonresponse.get('error', '')
            _cache['keys'] = keys_jsonresponse.get('keys', [])
        formuuid = hashlib.md5(str(random.random())).hexdigest()
        perpage = int(request.REQUEST.get("_perpage", "5"))
        offset = int(request.REQUEST.get("_offset", "0"))
        page = int(request.REQUEST.get("page", "-1"))

        if page > -1:
            offset = (page - 1) * perpage
        else:
            page = (offset / perpage) + 1
        pages = int(math.ceil(len(_cache['keys']) / perpage))
        return {
            'error': error,
            'keys': _cache['keys'][offset: (offset + perpage)],
            'query': self.default_request,
            'perpage': perpage,
            'pages': pages,
            'previous': page - 1,
            'next': page + 1,
            'page': page,
            'page_range': range(max(1, page - 3), min(pages, page + 3)),
            "view_url": self.get_view_url(),
            'formuuid': formuuid
        }

    def is_usable(self):
        for dependency in self.dependencies.all():
            if dependency.status != DONE:
                return False
        return True


MODELS.append(Dataset)
MODELS += get_dependent_models(Dataset)


# caltech256(128)#sift
# WE HAVE TO FORMALISE A CONCEPT OF FUNCTIONAL DETERMINISTIC DATASET EXPRESSIONS
##
# TODO : FIX ME SHOULD BE IN A CODE RELATED MODULE
@wideiomodel
@wideio_timestamped
class Function(models.Model):
    ##
    # Found automatically each time someone uploads a package
    ##
    #from science.models import AssemblyFile
    package = models.ForeignKey('software.AssemblyFile')
    function = models.TextField(max_length=64)  # NAME OF THE FUNCTION
    # TYPE OF THE FUNCTION ("aggregation_function","distance_function")
    tag = models.CharField(max_length=64)

    def __unicode__(self):
        return self.function

    class WIDEIO_Meta:
        permissions = dec.perm_read_logged_users_write_for_admin_only

MODELS.append(Function)
MODELS += get_dependent_models(Function)


@wideiomodel
@wideio_owned()
@wideio_timestamped
class EvaluationProtocol(models.Model):

    """
    """
    dataset = models.TextField(
    )                   # << Dataset expression TODO: A special widget must be used
    # << Obtention des resultats de reference
    ground_truth = models.TextField(null=True)
    evaltype = models.CharField(
        max_length=1,
        choices=(
            ('A',
             'Automated'),
            ('S',
             'Subjective'),
            ('B ',
             'Arbitrary')))  # SUBJECTIVE EVALUATION ??
    # A : Automated
    # B : Arbitrary, single person decides on the score
    # S : Users on the website are invited to give their opinon.
    # , query_set=("evaluation_function"))      #< only if not automated...
    distance_function = models.ForeignKey(
        Function,
        related_name="measuring_distance_in")
    aggregation_function = models.ForeignKey(
        Function,
        related_name="aggregating_data_in")  # , query_set=("aggregation_function"))

    def __unicode__(self):
        return self.function

    class WIDEIO_Meta:
        permissions = dec.perm_read_logged_users_write_for_admin_only
MODELS.append(EvaluationProtocol)
MODELS += get_dependent_models(EvaluationProtocol)


@wideiomodel
@wideio_owned()
@wideio_timestamped
class Evaluation(models.Model):

    """
    Associates a test protocol to a type of problem.
    A test campaign.
    """
    protocol_used = models.ForeignKey(EvaluationProtocol)
    evaluation_for = models.ForeignKey('science.Question')
    target_evaluation_models = models.ManyToManyField('science.Algorithm')

    status = models.CharField(max_length=1, null=True)
    #associated_runorder = models.ManyToManyField()

    # basic analytics on all the jobs..
    computation_started = models.DateTimeField(null=True)
    computation_ended = models.DateTimeField(null=True)
    cpu_time_spent = models.FloatField(null=True)  # Seconds

    def __unicode__(self):
        return self.function

    class WIDEIO_Meta:
        permissions = dec.perm_read_logged_users_write_for_admin_only

MODELS.append(Evaluation)
MODELS += get_dependent_models(Evaluation)


@wideiomodel
@wideio_timestamped
class EvaluationResult(models.Model):

    """
    Specific to a model.
    """
    protocol_used = models.ForeignKey(Evaluation)

    model_evaluated = models.ForeignKey('science.Algorithm')

    def __unicode__(self):
        return self.function

    class WIDEIO_Meta:
        permissions = dec.perm_read_logged_users_write_for_admin_only

MODELS.append(EvaluationResult)
MODELS += get_dependent_models(EvaluationResult)


@wideiomodel
@wideio_timestamped
class SubjectiveEvaluationCampaign(models.Model):

    """
    Specfic to a model
    """
    result_set = models.ForeignKey(EvaluationResult)
    minimum_results = models.IntegerField(default=1)
    maximum_duration = models.IntegerField(default=31)  # (days)
    maximum_votes_per_user = models.IntegerField(default=10000)

    class WIDEIO_Meta:
        permissions = dec.perm_read_logged_users_write_for_admin_only

MODELS.append(SubjectiveEvaluationCampaign)
MODELS += get_dependent_models(SubjectiveEvaluationCampaign)


@wideiomodel
@wideio_timestamped
class SubjectiveEvaluationVote(models.Model):

    """
    Specific to a model
    """
    user = models.ForeignKey('accounts.UserAccount')
    rating = models.FloatField()
    # FIXME  temporal / complex rating (may need specific apps/extensions)
    advanced_rating = models.TextField()
    campaign = models.ForeignKey(SubjectiveEvaluationCampaign)
    data_address = models.TextField()

    class WIDEIO_Meta:
        permissions = dec.perm_read_logged_users_write_for_admin_only

MODELS.append(SubjectiveEvaluationVote)
MODELS += get_dependent_models(SubjectiveEvaluationVote)

# TODO : SUBJECTIVE EVALUATION RESULTS & CAMPAIGN
